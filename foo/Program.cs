﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foo
{
    class Program
    {
        static void Main(string[] args)
        {
            //var lines = string.Format("00:01:57,400 - 234 - 090{0}00:05:01,701 - 080 - 080{0}00:05:00,400 - 234 - 090", Environment.NewLine);
            //var price = solution(lines);

            //int[] intArray = new int[] { 8, 24, 3, 20, 1, 17 };
            //solution(intArray);
            var s = solution(4, 17);
        }

        public class BillingItem
        {
            public static int TariffPerSecond = 3;
            public static int TariffPerMinute = 150;
            public static int SecondsInFiveMinutes = 60 * 5;

            public TimeSpan Duration { get; private set; }

            public string Number { get; private set; }

            public int Price { get; private set; }

            public BillingItem(string line)
            {
                var items = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                this.Duration = TimeSpan.ParseExact(items[0], "hh\\:mm\\:ss", System.Globalization.CultureInfo.InvariantCulture);
                this.Number = items[1];
                this.Price = this.GetPrice();
            }

            protected int GetPrice()
            {
                //Math.Ceiling(Duration.TotalMinutes)
                if (Duration.TotalMinutes < 5)
                {
                    return (int)Duration.TotalSeconds * TariffPerSecond;
                }
                else if (Duration.TotalSeconds == SecondsInFiveMinutes)
                {
                    return (int)Duration.TotalMinutes * TariffPerMinute;
                }
                else
                {
                    return ((int)Duration.TotalMinutes + 1) * TariffPerMinute;
                }
            }
        }

        public class BillingEqualityComparer : IEqualityComparer<BillingItem>
        {
            public bool Equals(BillingItem x, BillingItem y)
            {
                return string.Equals(x.Number, y.Number);
            }

            public int GetHashCode(BillingItem obj)
            {
                return obj.Price.GetHashCode();
            }
        }

        public static int solution(string A)
        {
            var result = 0;
            var lines = A.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var billingItems = lines.Select(line => new BillingItem(line));

            var numbers = new Dictionary<string, BillingItem>();
            foreach(var billing in billingItems)
            {
            }

            var s = numbers.Values.OrderBy(v => v.Number).ToArray();
            var n = s.Length;
            var dd = s.Take(n - 1);
            //result = dd.Sum(s => s.Price);
            return result;
        }

        public static int solution(int[] A)
        {
            // the task is incomplete it does not tell me what to do it we have wrong input,
            // i assume i will raise an exception
            var result = Int32.MaxValue;

            Array.Sort(A);
            for (int i = 0; i < A.Length - 1; i++)
            {
                var distance = A[i + 1] - A[i];
                if (distance < result)
                    result = distance;
            }

            return result;
        }

        public static int solution(int A, int B)
        {
            return (int)(Math.Sqrt(B+1) - Math.Sqrt(A));

            var result = 0;
            A = A < 0 ? 0 : A;
            if (A <= B)
            {
                for (int i = A; i <= B; i++)
                {
                    var square = Math.Sqrt(i);
                    var delta = square - (int)square;
                    if (delta <= 0.00001d)
                    {
                        result++;
                    }
                }
            }
            System.Diagnostics.Debug.Assert((int)(Math.Sqrt(B) - Math.Sqrt(A))+1 == result);
            return result;
        }
    }
}

