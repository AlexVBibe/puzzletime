﻿using System;
using System.Linq;

namespace NameThatRabbit
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("----Input, like: (alex bob john): ");
                var input = Console.ReadLine();
                var names = input.
                    Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).
                    Select(name => name.ToLower()).
                    ToList();

                names.Sort((n1, n2) => Zip(n2) - Zip(n1));

                Console.WriteLine(string.Format("Names: ({0})", string.Join(", ", names)));
                Console.WriteLine("Again, y/n?");
            } while (Console.ReadLine() == "y");
        }

        public static int Zip(string word)
        {
            var symbols = word.ToArray();
            var sum = symbols.Sum(ch => (int)ch - (int)'a');
            return sum;
        }
    }
}
