﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HashItOut
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("----Input, sequence (1 129 3 129): ");
                var input = Console.ReadLine();
                var hashCode = input.
                    Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).
                    Select(number => byte.Parse(number)).
                    ToArray();

                var result = new List<byte>(hashCode.Length);
                for(int i = 0; i < hashCode.Length; i++)
                {
                    var input1 = hashCode[i];
                    var input2 = i == 0 ? (byte)0 : hashCode[i - 1];
                    result.Add(GetHash(input1, input2));
                }

                Console.WriteLine(string.Format("Code: ({0})", string.Join(", ", result)));
                Console.WriteLine("Again, y/n?");
            } while (Console.ReadLine() == "y");
        }

        public static byte GetHash(byte value1, byte value2)
        {
            var s = (byte)(((129 * value1) ^ value2) % 256);
            return s;
        }
    }
}
